package cn;

import java.rmi.*;
import java.rmi.registry.LocateRegistry;

public class DeployServer{
    public DeployServer(){
}

    public static void main(String[] args) {
        try{
            DataService ds = new DataServiceImpl();
            LocateRegistry.createRegistry(8099);
            Naming.rebind("//localhost:8099/ds",ds);
            System.out.println("RMI服务器正在运行...");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
